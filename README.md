# Carbon certificates

## Carbon Certificates application API
Create the API containing endpoints:

1. Login
2. List of available Carbon certificates (no owner)
3. List of owned Carbon certificates (owned by current user)
4. Transfer my own Carbon certificate to the another existing user (based on the User ID parameter)

### Data informations
Carbon certificate should contain the following data:
- Unique ID
- Country
- Status:
available (no owner)
owned (owner is present and certificate hasn't been transferred)
transferred (owner is present and certificate has been transferred from one owner to another)
Owner (relation to existing user, can be empty)

### Requirements
1. Application should be written with strong typing (TypeScript)
2. Framework is free of choice
3. Authentication should be implemented (type/package if free of choice)
4. Seeds should be included (100 random certificates, 5 random users with certificates and 5 without them)
5. Tests have to be included

How to start?
1. npm i
2. Be sure that you have Docker and Docker-compose
3. npm start
4. If you want to run tests, just jest
