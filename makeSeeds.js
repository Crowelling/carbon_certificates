/* mySeedScript.js */

// require the necessary libraries
const faker = require("faker");
const MongoClient = require("mongodb").MongoClient;

function randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min);
}

async function seedDB() {
    // Connection URL
    const uri = "mongodb://localhost:27017/";

    const client = new MongoClient(uri, {
        useNewUrlParser: true,
        // useUnifiedTopology: true,
    });

    try {
        await client.connect();
        console.log("Connected correctly to server");

        const collectionCertificates = client.db("api-db").collection("Certificates");
        const collectionUsers = client.db("api-db").collection("Users");

        // The drop() command destroys all data from a collection.
        // Make sure you run it against proper database and collection.
        collectionCertificates.drop();
        collectionUsers.drop();

        // make a bunch of time series data
        let collectionsData = [];
        let usersData = [];

        for (let i = 0; i < 10; i++) {
            usersData.push({
                _id: i,
                email: faker.email(),
                password: faker.password(),
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName(),
            });
            collectionsData.push({
                country: faker.country(),
                status: !(i % 2) ? 'owned' : 'available',
                owner: !(i % 2) ? i : undefined,
            })
        }

        for (let i = 0; i < 90; i++) {
            collectionsData.push({
                country: faker.country(),
                status: 'available',
                owner: undefined,
            })
        }
        collectionCertificates.insertMany(collectionsData);
        collectionUsers.insertMany(usersData);

        console.log("Database seeded! :)");
        client.close();
    } catch (err) {
        console.log(err.stack);
    }
}

seedDB();
