import express from 'express';
import { CommonRoutesConfig } from '../common/common.routes.config';
import CertificatesController from './controllers/certificates.controller';
import CertificatesMiddleware from './middleware/certificates.middleware';
import BodyValidationMiddleware from '../common/middleware/body.validation.middleware';
import jwtMiddleware from '../auth/middleware/jwt.middleware';
import UsersMiddleware from './middleware/users.middleware';

export class CertificatesRoutes extends CommonRoutesConfig {
    constructor(app: express.Application) {
        super(app, 'CertificatesRoutes');
    }

    configureRoutes() {
        this.app
            .route(`/certificates`)
            .get(
                jwtMiddleware.validJWTNeeded,
                CertificatesController.listFreeCertificates,
            );

        this.app
            .route(`/certificates/:userId`)
            .get(
                jwtMiddleware.validJWTNeeded,
                UsersMiddleware.validateUserExists,
                CertificatesController.listOwnedCertificates,
            );

        this.app
            .put(`/certificates/:userId`, [
                jwtMiddleware.validJWTNeeded,
                UsersMiddleware.validateUserExists,
                BodyValidationMiddleware.verifyBodyFieldsErrors,
                BodyValidationMiddleware.verifyBodyTransferCertificate,
                CertificatesMiddleware.validateCertificate,
                CertificatesController.transferCertificate,
            ]);

        return this.app;
    }
}
