import express from 'express';
import debug from 'debug';
import CertificatesService from '../services/certificates.service';

const log: debug.IDebugger = debug('app:certificates-controller');

class CertificatesController {
    async listFreeCertificates(req: express.Request, res: express.Response) {
        const certificates = await CertificatesService.listFree(100, 0);
        res.status(200).send(certificates);
    }

    async listOwnedCertificates(req: express.Request, res: express.Response) {
        const { userId } = req.params;
        const certificates = await CertificatesService.listOwned(userId);
        res.status(200).send(certificates);
    }

    async transferCertificate(req: express.Request, res: express.Response) {
        const { certId, newOwner } = req.body;
        await CertificatesService.transferCertificate({ certId, newOwner });
        res.status(204).send();
    }
}

export default new CertificatesController();
