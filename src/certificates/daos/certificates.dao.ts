import debug from 'debug';
import mongooseService from '../../common/services/mongoose.service';
import { TransferCertificateInterface } from '../../common/interfaces/certificates.interface';

const log: debug.IDebugger = debug('app:certificates-dao');

export enum CertificateStatusEnum {
    AVAILABLE = 'available',
    OWNED = 'owned',
    TRANSFERRED = 'transferred',
}

class CertificatesDao {
    Schema = mongooseService.getMongoose().Schema;
    certificateSchema = new this.Schema({
        _id: String,
        country: String,
        status: { type: String, enum: CertificateStatusEnum },
        owner: String,
    }, { id: false });
    Certificate = mongooseService.getMongoose().model('Certificates', this.certificateSchema);

    constructor() {
        log('Created new instance of CertificatesDao');
    }
    
    async getFreeCertificates(limit = 25, page = 0) {
        return await this.Certificate.find({ status: CertificateStatusEnum.AVAILABLE })
            .limit(limit)
            .skip(limit * page)
            .exec();
    }

    async getOwnedCertificates(userId: string) {
        return await this.Certificate.find({ owner: userId }).exec()
    }

    async transferCertificate({ certId, newOwner }: TransferCertificateInterface) {
        return await this.Certificate.findOneAndUpdate(
            { _id: certId },
            { $set: { status: CertificateStatusEnum.TRANSFERRED, owner: newOwner } },
            { new: true },
        ).exec();
    }
}

export default new CertificatesDao();
