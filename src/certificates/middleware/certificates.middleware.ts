import express from 'express';
import debug from 'debug';
import CertificatesService from '../services/certificates.service';
import UsersService from '../services/users.service';

const log: debug.IDebugger = debug('app:certificates-middleware');

class CertificatesMiddleware {
    private async validateCertificateBelongsToUser(
        { owner, certId }: { owner: string, certId: string },
        res: express.Response,
        next: express.NextFunction
    ) {
        const certificates = await CertificatesService.listOwned(owner);
        const certificateBelongsToUser = certificates.find((certificate: any) => certificate._id == certId);
        if (!certificateBelongsToUser) {
            res.status(400).send({ error: `certificate doesn't belong to current user` });
        }
        next();
    }
    
    private async validateNewOwnerExists(
        newOwner: string,
        res: express.Response,
        next: express.NextFunction
    ) {
        if (!(await UsersService.getUser(newOwner))) {
            res.status(400).send({ error: `Invalid newOwner` });
        }
        next();
    }

    async validateCertificate (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const { certId, newOwner } = req.body;
        const { userId: owner } = req.params;
        log(`Validating certificate: { certId: ${certId}, owner: ${owner} }`);

        await this.validateCertificateBelongsToUser({ owner, certId }, res, next);
        await this.validateNewOwnerExists(newOwner, res, next);
    }
}

export default new CertificatesMiddleware();
