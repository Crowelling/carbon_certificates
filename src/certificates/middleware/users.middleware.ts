import express from 'express';
import UsersService from '../services/users.service';
import debug from 'debug';

const log: debug.IDebugger = debug('app:users-middleware');

class UsersMiddleware {
    async validateUserExists(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const user = await UsersService.getUser(req.params.userId);
        if (user) {
            next();
        } else {
            res.status(404).send({
                error: `User ${req.params.userId} not found`,
            });
        }
    }
}

export default new UsersMiddleware();
