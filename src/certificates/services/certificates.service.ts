import CertificatesDao from '../daos/certificates.dao';
import {
    CertificatesServiceInterface,
    TransferCertificateInterface,
} from '../../common/interfaces/certificates.interface';

class CertificatesService implements CertificatesServiceInterface {
    async listFree(limit: number, page: number) {
        return CertificatesDao.getFreeCertificates(limit, page);
    }

    async listOwned(userId: string): Promise<any> {
        return CertificatesDao.getOwnedCertificates(userId);
    }

    async transferCertificate({ certId, newOwner }: TransferCertificateInterface): Promise<any> {
        return CertificatesDao.transferCertificate({ certId, newOwner });
    }
}

export default new CertificatesService();
