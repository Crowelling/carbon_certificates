import { UsersServiceInterface } from '../../common/interfaces/users.interface';
import UsersDao from '../daos/users.dao';

class UsersService implements UsersServiceInterface {
    async getUser(id: string) {
        return await UsersDao.getUserById(id);
    }

    async getUserByEmailWithPassword(email: string) {
        return await UsersDao.getUserByEmailWithPassword(email);
    }
}

export default new UsersService();
