export interface TransferCertificateInterface {
    certId: string
    newOwner: string
}

export interface CertificatesServiceInterface {
    listFree: (limit: number, page: number) => Promise<any>;
    listOwned: (userId: string) => Promise<string>;
    transferCertificate: ({ certId, newOwner }: TransferCertificateInterface) => Promise<string>;
}
