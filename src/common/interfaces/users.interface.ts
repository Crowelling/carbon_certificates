export interface UsersServiceInterface {
    getUser: (id: string) => Promise<any>;
    getUserByEmailWithPassword: (email: string) => Promise<any>;
}
