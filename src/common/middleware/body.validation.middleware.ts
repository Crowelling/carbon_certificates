import express from 'express';
import { validationResult } from 'express-validator';

class BodyValidationMiddleware {
    verifyBodyFieldsErrors(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).send({ errors: errors.array() });
        }
        next();
    }

    verifyBodyTransferCertificate(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const { certId, newOwner } = req.body;
        if (!certId || !newOwner) {
            res.status(400).send({ error: `Invalid body` });
        }
        next();
    }
}

export default new BodyValidationMiddleware();
