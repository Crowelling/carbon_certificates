import { getMockReq, getMockRes } from '@jest-mock/express';

const userId = '1111';
const newOwner = '2222';
const listFreeMock = {
    _id: 'some1234',
    country: 'USA',
    status: 'available',
    owner: undefined,
};
const listOwnedMock = {
    _id: 'some4567',
    country: 'USA',
    status: 'owned',
    owner: userId,
};

jest.mock('../../../src/certificates/services/certificates.service', () => ({
    listFree: jest.fn().mockImplementation(() => ({
        _id: 'some1234',
        country: 'USA',
        status: 'available',
        owner: undefined,
    })),
    listOwned: jest.fn().mockImplementation(() => ({
        _id: 'some4567',
        country: 'USA',
        status: 'owned',
        owner: userId,
    })),
    transferCertificate: jest.fn().mockImplementation(),
}));

import CertificatesService from '../../../src/certificates/services/certificates.service';
import CertificatesController from "../../../src/certificates/controllers/certificates.controller";

describe('Certificates controller tests', () => {
    beforeEach(() => jest.clearAllMocks());
    it('Check success listFreeCertificates return', async () => {
        const req = getMockReq();
        const res = getMockRes().res;
        const result = await CertificatesController.listFreeCertificates(req, res);
        expect(res.send).toHaveBeenCalledWith(listFreeMock);
    });
    it('Check success listOwnedCertificates return', async () => {
        const req = getMockReq({ params: { userId } });
        const res = getMockRes().res;
        const result = await CertificatesController.listOwnedCertificates(req, res);
        expect(jest.spyOn(CertificatesService, 'listOwned')).toHaveBeenCalledWith(userId);
        expect(res.send).toHaveBeenCalledWith(listOwnedMock);
    });
    it('Check success transferCertificate return', async () => {
        const req = getMockReq({ body: { certId: listOwnedMock._id, newOwner } });
        const res = getMockRes().res;
        const result = await CertificatesController.transferCertificate(req, res);
        expect(jest.spyOn(CertificatesService, 'transferCertificate')).toHaveBeenCalledWith({
            certId: listOwnedMock._id,
            newOwner,
        });
        expect(res.send).toHaveBeenCalledWith();
    });
});
