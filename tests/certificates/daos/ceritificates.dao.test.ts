// @ts-expect-error TS7016
import * as mockingoose from 'mockingoose'

const userId = '1111';
const newOwner = '2222';
const listFreeMock = {
    _id: 'some1234',
    country: 'USA',
    status: 'available',
    owner: 'undefined',
};
const listOwnedMock = {
    _id: 'some4567',
    country: 'USA',
    status: 'owned',
    owner: userId,
};
const listTranferredMock = {
    _id: 'some4567',
    country: 'USA',
    status: 'transferred',
    owner: newOwner,
};

import CertificatesDao from '../../../src/certificates/daos/certificates.dao';

describe('Certificates dao tests', () => {
    beforeEach(() => jest.clearAllMocks());
    it('Check success getFreeCertificates return', async () => {
        mockingoose(CertificatesDao.Certificate).toReturn([listFreeMock], 'find');
        const limit = 100;
        const page = 1;
        const result = await CertificatesDao.getFreeCertificates(limit, page);
        expect(result[0]._id).toBe(listFreeMock._id);
        expect(result[0].country).toBe(listFreeMock.country);
        expect(result[0].status).toBe(listFreeMock.status);
        expect(result[0].owner).toBe(listFreeMock.owner);
    });
    it('Check success getOwnedCertificates return', async () => {
        mockingoose(CertificatesDao.Certificate).toReturn([listOwnedMock], 'find');
        const result = await CertificatesDao.getOwnedCertificates(userId);
        expect(result[0]._id).toBe(listOwnedMock._id);
        expect(result[0].country).toBe(listOwnedMock.country);
        expect(result[0].status).toBe(listOwnedMock.status);
        expect(result[0].owner).toBe(listOwnedMock.owner);
    });
    it('Check success transferCertificate return', async () => {
        mockingoose(CertificatesDao.Certificate).toReturn([listTranferredMock], 'findOneAndUpdate');
        const result = await CertificatesDao.transferCertificate({ certId: listOwnedMock._id, newOwner });
        expect(result[0]._id).toBe(listTranferredMock._id);
        expect(result[0].country).toBe(listTranferredMock.country);
        expect(result[0].status).toBe(listTranferredMock.status);
        expect(result[0].owner).toBe(listTranferredMock.owner);
    });
});
