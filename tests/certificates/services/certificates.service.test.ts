const userId = '1111';
const newOwner = '2222';
const listFreeMock = {
    _id: 'some1234',
    country: 'USA',
    status: 'available',
    owner: undefined,
};
const listOwnedMock = {
    _id: 'some4567',
    country: 'USA',
    status: 'owned',
    owner: userId,
};
const listTranferredMock = {
    _id: 'some4567',
    country: 'USA',
    status: 'transferred',
    owner: newOwner,
};

jest.mock('../../../src/certificates/daos/certificates.dao', () => ({
    getFreeCertificates: jest.fn().mockImplementation(() => ({
        _id: 'some1234',
        country: 'USA',
        status: 'available',
        owner: undefined,
    })),
    getOwnedCertificates: jest.fn().mockImplementation(() => ({
        _id: 'some4567',
        country: 'USA',
        status: 'owned',
        owner: userId,
    })),
    transferCertificate: jest.fn().mockImplementation(() => ({
        _id: 'some4567',
        country: 'USA',
        status: 'transferred',
        owner: newOwner,
    })),
}));

import CertificatesDao from '../../../src/certificates/daos/certificates.dao';
import CertificatesService from "../../../src/certificates/services/certificates.service";

describe('Certificates services tests', () => {
    beforeEach(() => jest.clearAllMocks());
    it('Check success listFree return', async () => {
        const limit = 100;
        const page = 1;
        const result = await CertificatesService.listFree(limit, page);
        expect(jest.spyOn(CertificatesDao, 'getFreeCertificates')).toHaveBeenCalledWith(limit, page);
        expect(result).toEqual(listFreeMock);
    });
    it('Check success listOwned return', async () => {
        const result = await CertificatesService.listOwned(userId);
        expect(jest.spyOn(CertificatesDao, 'getOwnedCertificates')).toHaveBeenCalledWith(userId);
        expect(result).toEqual(listOwnedMock);
    });
    it('Check success transferCertificate return', async () => {
        const result = await CertificatesService.transferCertificate({
            certId: listOwnedMock._id,
            newOwner,
        });
        expect(jest.spyOn(CertificatesDao, 'transferCertificate')).toHaveBeenCalledWith({
            certId: listOwnedMock._id,
            newOwner,
        });
        expect(result).toEqual(listTranferredMock);
    });
});
